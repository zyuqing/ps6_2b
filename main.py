# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np
import plotly.express as px
flights = pd.read_csv("flights.csv.gz")

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
series = flights.groupby("carrier")["dep_delay"].mean().sort_values()[::-1]
names = list(series.index)
value = pd.Series(series.values)
df = pd.DataFrame({
    "Average delay time": value,
    "Carrier": names
})
fig_1 = px.bar(df, y="Average delay time", x="Carrier", title = 'The average delay times of different carriers')

series = flights.groupby("month")["dep_delay"].mean()
names = list(series.index)
value = pd.Series(series.values)
df = pd.DataFrame({
    "Average delay time": value,
    "Month": names
})
fig_2 = px.bar(df, y="Average delay time", x="Month", title = 'The average delay times of different months')

app.layout = html.Div(children=[
    html.H1(children='Delay time simple analysis'),

    html.Div(children='''
        When we look into different carriers, we can see that there is clear difference among the average delay time. F9, EV and other carriers seem to have more severe delay situations. It may help us choose a suitable carrier.
    '''),

    dcc.Graph(
        id='delay_carrier',
        figure=fig_1
    ),

    html.Div(children='''
        When we analyze the delay time within different months, we can find that the delay is the most severe in June and July. And it also seems that September, October and November have the least delay situations. 
    '''),

    dcc.Graph(
        id='delay_month',
        figure=fig_2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
